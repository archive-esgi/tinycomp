const _routes = new WeakMap();
const _componentsDirectory = new WeakMap();
const _baseTitle = new WeakMap();
const _currentMainComponent = new WeakMap();

export default class Router {
  constructor(routes, componentsDirectory, baseTitle) {
    _routes.set(this, routes);
    _componentsDirectory.set(this, componentsDirectory);
    _baseTitle.set(this, baseTitle || 'TinyComp');
    _currentMainComponent.set(this, null);
  }

  /**
   * Set the app component
   * @param {Component} app
   */
  setApp(app) {
    this.app = app;
  }

  /**
   * Update DOM with a main component
   * @param {Component} component
   */
  async _update(component) {
    this.app.eraseChildren();

    try {
      const componentPath = `${_componentsDirectory.get(this)}/${component}.js`;
      _currentMainComponent.set(this, (await import(componentPath)).default);
      _currentMainComponent.get(this).generate();
    } catch (e) {
      try {
        const componentPath = `./components/${component}.js`;
        _currentMainComponent.set(this, (await import(componentPath)).default);
        _currentMainComponent.get(this).generate(this.app);
      } catch (ew) {
        this.app.createElement('h1', null, 'Error 500');
      }
    }

    this.app.display();
  }

  /**
   * Update DOM by URI
   */
  updateByUri() {
    const route = _routes.get(this).find((routeWatch) => this.isCurrentUri(routeWatch.path));

    if (!route) {
      document.title = this.getFullTitle('Error 404');
      this._update('errors/404');
      return;
    }

    document.title = this.getFullTitle(route.title);
    this._update(route.component);
  }

  /**
   * Check if is the current URI
   * @param {string} path
   */
  isCurrentUri(path) {
    return window.location.pathname === path;
  }

  /**
   * Check if is the current host
   * @param {string} host
   */
  isCurrentHost(host) {
    return host.startsWith(window.location.origin);
  }

  /**
   * Get route by name
   * @param {string} name
   */
  getRouteByName(name) {
    return _routes.get(this).find((route) => route.name === name);
  }

  /**
   * Get pth by route name
   * @param {string} name
   */
  getPathByRouteName(name) {
    const route = this.getRouteByName(name);
    if (!route) {
      return route;
    }

    return route.path;
  }

  /**
   * Get route by path
   * @param {string} path
   */
  getRouteByPath(path) {
    return _routes.get(this).find((route) => route.path === path);
  }

  /**
   * Get full title
   * @param {string} extraTitle
   */
  getFullTitle(extraTitle) {
    return extraTitle ? `${extraTitle} - ${_baseTitle.get(this)}` : _baseTitle.get(this);
  }

  /**
   * Launch all router listners
   */
  launchListners() {
    this._clickOnATag();
    this._historyListner();
  }

  /**
   * Listner on click on a tag
   */
  _clickOnATag() {
    document.addEventListener('click', (event) => {
      if (event.target.localName !== 'a' || !event.target.href) {
        return;
      }

      event.preventDefault();

      const { target } = event;

      if (!this.isCurrentHost(target.href)) {
        return;
      }

      const pathTarget = target.href.replace(window.location.origin, '');
      const route = this.getRouteByPath(pathTarget);
      let title = '';
      let component = '';

      if (route === undefined) {
        title = 'Error 404';
        component = 'errors/404';
      } else {
        title = route.title === undefined ? '' : route.title;
        component = route.component;
      }

      title = this.getFullTitle(title);

      window.history.pushState({ }, title, target.href);
      document.title = title;

      this._update(component);
    });
  }

  /**
   * Listner on history moving
   */
  _historyListner() {
    window.onpopstate = () => {
      this.updateByUri();
    };
  }
}
