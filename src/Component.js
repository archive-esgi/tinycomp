export default class Component {
  constructor(type, props, parent, ...children) {
    this.type = type;
    this.props = {
      ...props,
      children: children.map((child) => {
        const childComponent = typeof child === 'object'
          ? child : new Component('TEXT_ELEMENT', { nodeValue: child }, this);
        if (childComponent.parent !== null && childComponent.type !== 'TEXT_ELEMENT') {
          childComponent.parent.removeChild(child);
          childComponent.parent = this;
        }
        childComponent.parent = this;
        return childComponent;
      }, this),
    };

    this.parent = parent;
    this.state = null;
  }

  /**
   * Get a copy of current props
   */
  getProps() {
    const temp = Object.assign([], this.props);
    temp.children = temp.children.map((child) => child);
    return temp;
  }

  /**
   * Remove a child
   * @param {Component} childToRemove
   */
  removeChild(childToRemove) {
    this.props.children = this.props.children.filter((child) => child !== childToRemove);
  }

  /**
   * Remove all children
   */
  eraseChildren() {
    this.props.children = [];
    let child = this.state.firstChild;
    while (child) {
      this.state.removeChild(child);
      child = this.state.firstChild;
    }
  }

  /**
   * Check update Component with new props
   * @param {Object} props
   */
  display(props) {
    if (this._shouldUpdate(props)) {
      this._render(this.parent.state);
    }
  }

  /**
   * Check if the Component need to be update
   * @param {Object} props
   */
  _shouldUpdate(props) {
    if (props !== this.props) {
      this.props = props;
      return true;
    }

    return false;
  }

  /**
   * Transform the virtual DOM into DOM
   * @param {Object} container
   */
  _render(container) {
    const dom = this.type === 'TEXT_ELEMENT'
      ? document.createTextNode('')
      : document.createElement(this.type);

    const isProperty = (key) => key !== 'children';
    Object.keys(this.props)
      .filter(isProperty)
      .forEach((name) => {
        if (dom.nodeType !== Node.TEXT_NODE) {
          dom.setAttribute(name, this.props[name]);
        } else {
          dom[name] = this.props[name];
        }
      });

    if (this.props.children.length > 0) {
      this.props.children.forEach((child) => {
        child._render(dom);
      });
    }

    const oldState = this.state;
    this.state = dom;

    if (oldState) {
      this.parent.state.replaceChild(this.state, oldState);
    } else {
      container.appendChild(dom);
    }
  }
}
